package DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;



import Model.Manifestacija;

public class ManifestacijaDAO {


	public static Manifestacija pronadjiManifestacijuPoId(Connection conn, int id) {
		Manifestacija manifestacija = null;
		try {
			Statement stmt = conn.createStatement();
			ResultSet rset = stmt
					.executeQuery("SELECT naziv, brPosetilaca " +
							"FROM manifestacije WHERE manifestacija_id = " 
							+ id);

			if (rset.next()) {
				String naziv = rset.getString(1);
				int brPosetilaca = rset.getInt(2);
				
				
				manifestacija = new Manifestacija(id, naziv, brPosetilaca);
			}
			rset.close();
			stmt.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return manifestacija;
	}

	public static boolean ObrisiManifestaciju(Connection conn, int id) {
		boolean retVal = false;
		try {
			String update = "DELETE FROM manifestacije WHERE manifestacija_id = " + id;
			Statement stmt = conn.createStatement();
			if(stmt.executeUpdate(update) == 1)
				retVal = true;
			stmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return retVal;
	}


	public static boolean dodajManifestaciju(Connection conn, Manifestacija manifestacija){
		boolean retVal = false;
		try {
			String update = "INSERT INTO manifestacje (naziv, brPosetilaca) values (?, ?)";
			PreparedStatement pstmt = conn.prepareStatement(update);
			pstmt.setString(1, manifestacija.getNaziv());
			pstmt.setInt(2, manifestacija.getBrPosetilaca());
			if(pstmt.executeUpdate() == 1){
				retVal = true;
			}
			pstmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return retVal;
	}
	
	public static boolean izmeniManifestaciju(Connection conn, Manifestacija manifestacija) {
		boolean retVal = false;
		try {
			String update = "UPDATE predmeti SET naziv=?, brPosetilaca WHERE manifestacija_id=?";
			PreparedStatement pstmt = conn.prepareStatement(update);
			pstmt.setString(1, manifestacija.getNaziv());
			pstmt.setInt(2, manifestacija.getBrPosetilaca());
			pstmt.setInt(3, manifestacija.getId());
			if(pstmt.executeUpdate() == 1)
				retVal = true;
			pstmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return retVal;
	}
	public static List<Manifestacija> prikazSvihManifestacija(Connection conn){
			List<Manifestacija> retVal = new ArrayList<Manifestacija>();
			try {
				String query = "SELECT id, naziv, brPosetilaca FROM manifestacije ";
				Statement stmt = conn.createStatement();
				ResultSet rset = stmt.executeQuery(query.toString());
				while (rset.next()) {
					int id = rset.getInt(1);
					String naziv = rset.getString(2);
					int brPosetilaca = rset.getInt(3);
					Manifestacija manifestacija = new Manifestacija(id, naziv, brPosetilaca);
					retVal.add(manifestacija);
				}
				rset.close();
				stmt.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
			return retVal;
	}		}