package Model;

public class Grad {

	
	protected int ptt;
	protected String naziv;
	
	
	
	public Grad() {
		
	}



	public Grad(int ptt, String naziv) {
		
		this.ptt = ptt;
		this.naziv = naziv;
	}



	public int getPtt() {
		return ptt;
	}



	public void setPtt(int ptt) {
		this.ptt = ptt;
	}



	public String getNaziv() {
		return naziv;
	}



	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}


	public String toString() {
		return "Grad ciji je ptt, " + ptt + ", ima naziv, " + naziv + "]";
	}



	
	
}
