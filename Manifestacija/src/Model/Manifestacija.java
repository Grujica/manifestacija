package Model;

public class Manifestacija {

	
	protected int id;
	protected String naziv;
	protected int brPosetilaca;
	protected Grad grad;
	
	public Grad getGrad() {
		return grad;
	}



	public void setGrad(Grad grad) {
		this.grad = grad;
	}



	public Manifestacija(int id, String naziv, int brPosetilaca) {
		
		this.id = id;
		this.naziv = naziv;
		this.brPosetilaca = brPosetilaca;
	}

	

	public Manifestacija(int id, String naziv, int brPosetilaca, Grad grad) {
		super();
		this.id = id;
		this.naziv = naziv;
		this.brPosetilaca = brPosetilaca;
		this.grad = grad;
	}



	public int getId() {
		return id;
	}

	

	public void setId(int id) {
		this.id = id;
	}


	public String getNaziv() {
		return naziv;
	}


	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}


	public int getBrPosetilaca() {
		return brPosetilaca;
	}


	public void setBrPosetilaca(int brPosetilaca) {
		this.brPosetilaca = brPosetilaca;
	}



	public String toString() {
		return "Manifestacija sa ID-em, " + id + " ,ima naziv - " + naziv
				+ ", broj posetilaca - " + brPosetilaca + "]";
	}


	
	

		
	}


	
	
	
