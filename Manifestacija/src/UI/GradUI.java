package UI;

import java.util.ArrayList;
import java.util.List;

import DAO.GradDAO;
import Model.Grad;
import PomKlasa.ScannerWrapper;

public class GradUI {

public static ArrayList<Grad> gradovi = new ArrayList<Grad>();
	
	public static void menu() {
		int odluka = -1;
		while (odluka != 0) {
			ispisiMenu();
			System.out.print("opcija:");
			odluka = ScannerWrapper.ocitajCeoBroj();
			switch (odluka) {
			case 0:
				System.out.println("Izlaz");
				break;
			case 1:
				ispisiSveGradove();
				break;
			case 2:
				unosNovogGrada();
				break;
			case 3:
				izmenaPodatakaOGradu();
				break;
			case 4:
				brisanjePodatakaOGradu();
				break;
			default:
				System.out.println("Nepostojeca komanda");
				break;
			}
		}
	}

	public static void ispisiMenu() {
		System.out.println("Rad sa gradovima - opcije:");
		System.out.println("\tOpcija broj 1 - ispis grada");
		System.out.println("\tOpcija broj 2 - unos novog  grada");
		System.out.println("\tOpcija broj 3 - izmena grada");
		System.out.println("\tOpcija broj 4 - brisanje grada");
		System.out.println("\t\t ...");
		System.out.println("\tOpcija broj 0 - IZLAZ");
	}

	
	public static void ispisiSveGradove() {
		List<Grad> gradovi = GradDAO.getAll(AplikacijaUI.conn);
		for (int i = 0; i < gradovi.size(); i++) {
			System.out.println(gradovi.get(i));
		}
	}

	
	public static Grad pronadjiGrad() {
		Grad retVal = null;
		System.out.print("Unesi naziv:");
		String grNaziv = ScannerWrapper.ocitajTekst();
		retVal = pronadjiGrad(grNaziv);
		if (retVal == null)
			System.out.println("Manifestacija sa nazivom " + grNaziv
					+ " ne postoji u evidenciji");
		return retVal;
	}


		public static Grad pronadjiGrad(String grNaziv) {
			Grad retVal = GradDAO.getGradByNaziv(AplikacijaUI.conn,
					grNaziv);
			return retVal;
		}

	
	public static void unosNovogGrada() {
		System.out.print("Unesi naziv:");
		String grNaziv = ScannerWrapper.ocitajTekst();
		grNaziv = grNaziv.toUpperCase();
		while (pronadjiGrad(grNaziv) != null) {
			System.out.println("Grad sa Naizvom " + grNaziv
					+ " vec postoji");
			grNaziv = ScannerWrapper.ocitajTekst();
		}
		
		System.out.print("Unesi PTT:");
		int grPtt = ScannerWrapper.ocitajCeoBroj();
		
		
		Grad gr = new Grad(grPtt, grNaziv);
		
		GradDAO.add(AplikacijaUI.conn, gr);
	}

	
	public static void izmenaPodatakaOGradu() {
		Grad gr = pronadjiGrad();
		if (gr != null) {
			System.out.print("Unesi novi naziv :");
			String grNaziv = ScannerWrapper.ocitajTekst();
			gr.setNaziv(grNaziv);

			System.out.print("Unesi PTT:");
			int ptt = ScannerWrapper.ocitajCeoBroj();
			gr.setPtt(ptt);
	
			GradDAO.update(AplikacijaUI.conn, gr);
		}
	}

	
	
	public static void brisanjePodatakaOGradu() {
		Grad gr = pronadjiGrad();
		if (gr != null) {
			GradDAO.delete(AplikacijaUI.conn, gr.getPtt());
		}
	}

	
}


