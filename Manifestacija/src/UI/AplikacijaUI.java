package UI;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;



import UI.ManifestacijaUI;
import PomKlasa.ScannerWrapper;

public class AplikacijaUI {

public static Connection conn;
	
	static {
		try {
			// ucitavanje MySQL drajvera
			Class.forName("com.mysql.jdbc.Driver");

			// konekcija
			conn = DriverManager.getConnection(
					"jdbc:mysql://localhost:3306/kultura", 
					"root", "grujo");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	
	
	// ispis teksta osnovnih opcija
	public static void ispisiMenu() {
		System.out.println("Kulturna Manifestacija - Osnovne opcije:");
		System.out.println("\tOpcija broj 1 - Rad sa manfiestacijama");
		System.out.println("\tOpcija broj 2 - Rad sa gradovima");
		System.out.println("\t\t ...");
		System.out.println("\tOpcija broj 0 - IZLAZ IZ PROGRAMA");
	}

	

	public static void main(String[] args) throws IOException  {
		int odluka = -1;
		while (odluka != 0) {
			AplikacijaUI.ispisiMenu();
			
			System.out.print("opcija:");
			odluka = ScannerWrapper.ocitajCeoBroj();
			
			switch (odluka) {
			case 0:
				System.out.println("Izlaz iz programa");
				break;
			case 1:
				ManifestacijaUI.meniManifestacijaUI();
				break;
			case 2:
				GradUI.menu();
				break;
			default:
				System.out.println("Nepostojeca komanda");
				break;
			}
		}
	}
}
       