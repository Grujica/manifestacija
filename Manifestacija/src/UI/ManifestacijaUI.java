package UI;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;


import PomKlasa.ScannerWrapper;
import DAO.GradDAO;
import DAO.ManifestacijaDAO;
import Model.Grad;
import Model.Manifestacija;


public class ManifestacijaUI {


public static void ispisiMeni(){
		
		System.out.println("-----------Meni-------------");
		System.out.println("Opcija 1 -Evidencija svih manifestacija  ");
		System.out.println("Opcija 2 -Pronalazenje manifestacije po ID broju ");
		System.out.println("Opcija 3 -Dodavanje nove manifestacije u evidenciju");
		System.out.println("Opcija 4 -Izmena naziva evidencije ");
		System.out.println("Opcija 5 -Izmena svih podataka evidencije ");
		System.out.println("Opcija 6 -Brisanje evidencije ");
	
		System.out.println("Opcija 0 - ");
	
		
	}

	public static void meniManifestacijaUI() throws IOException{	
		int odluka = -1;
		while(odluka!= 0){
			ispisiMeni();
			System.out.print("opcija:");
			odluka = ScannerWrapper.ocitajCeoBroj();
			switch (odluka) {
				case 0:	
					System.out.println("Izlaz");	
					break;
				case 1:	
					evidencijaSvihManifestacija();
					break;
				case 2:	
					pronadjiManifestaciju();	
					break;
				case 3:	
					dodajNovuManifestaciju();
					break;
				
					
				case 5:	
					izmeniManifestaciju();	
					break;
				case 6:	
					izbrisiManifestaciju();
					break;
				default:
					System.out.println("Nepostojeca komanda");
					break;	
			}
		}
	}

	
	public static void evidencijaSvihManifestacija(){
		
		List<Manifestacija> manifestacije = ManifestacijaDAO.prikazSvihManifestacija(AplikacijaUI.conn);
		for (Manifestacija manifestacija : manifestacije) {
			System.out.println(manifestacija);
		}
		
	}
	
	public static Manifestacija pronadjiManifestacijuPoID(int id){
		
		Manifestacija km = ManifestacijaDAO.pronadjiManifestacijuPoId(AplikacijaUI.conn, id);
		;
		return km;
	}
	
	public static Manifestacija pronadjiManifestaciju(){
		
		Manifestacija km = null;
		System.out.print("Unesi id ");
		int id = ScannerWrapper.ocitajCeoBroj();
		km = ManifestacijaDAO.pronadjiManifestacijuPoId(AplikacijaUI.conn, id);
		if(km == null)
			System.out.println("Manifestacija ne postoji");
		else System.out.println(km);
		return km;
		
	}
	
	
	public static void dodajNovuManifestaciju(){
		
		Manifestacija km = null;
		System.out.print("Unesi novi naziv Manifestacije");
		String naziv = ScannerWrapper.ocitajTekst();
		
		
		System.out.print("Unesi novi broj posetilaca");
		int brPosetilaca = ScannerWrapper.ocitajCeoBroj();
			
		km = new Manifestacija(0, naziv, brPosetilaca);
		
		
		boolean provera = ManifestacijaDAO.dodajManifestaciju(AplikacijaUI.conn, km);
		if(provera)
			System.out.println("Manifestacija je uspesno dodata u evidenciju");
		else
			System.out.println("Grska pri unosu podataka");
		
		
	}
	

	public static void izmeniManifestaciju(){
		
		Manifestacija km = pronadjiManifestaciju();
		if(km != null){
			
			System.out.print("Unesi novi naziv Manifestacije");
			String naziv = ScannerWrapper.ocitajTekst();
			km.setNaziv(naziv);
			
			System.out.print("Unesi novi broj posetilaca");
			int brPosetilaca = ScannerWrapper.ocitajCeoBroj();
			km.setBrPosetilaca(brPosetilaca);
			
			
			
			boolean provera = ManifestacijaDAO.izmeniManifestaciju(AplikacijaUI.conn, km);
			if(provera)
				System.out.println("Uspesno ste izmenili podateke");
			else
				System.out.println("Greska pri unosu podataka");
		}
	}
	
	
	public static void izbrisiManifestaciju(){
		
		Manifestacija km =pronadjiManifestaciju();
		if(km != null){
			boolean provera = ManifestacijaDAO.ObrisiManifestaciju(AplikacijaUI.conn, km.getId());
			
			if(provera){
				System.out.println("Manifestacija je izbrisana iz evidencije");	
			}else{
				System.out.println("Greska pri unosu podataka");
			}
		}
	}

	


}
	